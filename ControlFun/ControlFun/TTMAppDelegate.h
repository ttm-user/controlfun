//
//  TTMAppDelegate.h
//  ControlFun
//
//  Created by admin on 9/9/14.
//  Copyright (c) 2014 TriThucMoi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TTMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
