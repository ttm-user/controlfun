//
//  TTMViewController.m
//  ControlFun
//
//  Created by admin on 9/9/14.
//  Copyright (c) 2014 TriThucMoi. All rights reserved.
//

#import "TTMViewController.h"

@interface TTMViewController ()

@end

@implementation TTMViewController
@synthesize nameField;
@synthesize numberField;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    int progress = (int) roundf(self.slider.value);
    self.sliderLabel.text = [NSString stringWithFormat:@"%d", progress];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)textFieldDoneEditing:(id)sender {
    [sender resignFirstResponder];
}

- (IBAction)backgroundTap:(id)sender {
    [nameField resignFirstResponder];
    [numberField resignFirstResponder];
}

- (IBAction)sliderChanged:(id)sender {
    self.slider = (UISlider *) sender;
    int progress = (int) roundf(self.slider.value);
    self.sliderLabel.text = [NSString stringWithFormat:@"%d", progress];
}

- (IBAction) switchChanged:(id)sender {
    UISwitch *whichSwitch = (UISwitch *) sender;
    BOOL setting = whichSwitch.isOn;
    [self.leftSwitch setOn:setting animated:YES];
    [self.rightSwitch setOn:setting animated:YES];
}

- (IBAction)toggleControls:(id)sender {
    if([sender selectedSegmentIndex] == 0) {
        self.leftSwitch.hidden = NO;
        self.rightSwitch.hidden = NO;
        self.doSomethingButton.hidden = YES;
    } else {
        self.leftSwitch.hidden = YES;
        self.rightSwitch.hidden = YES;
        self.doSomethingButton.hidden = NO;
    }
}

- (IBAction)buttonPressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"Are you sure?"
                                  delegate:self
                                  cancelButtonTitle:@"No way!"
                                  destructiveButtonTitle:@"Yes, I'm sure"
                                  otherButtonTitles:@"abc",@"xyz", nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex != [actionSheet cancelButtonIndex]) {
        NSString *msg = nil;
        if (nameField.text.length>0) {
            msg = [[NSString alloc] initWithFormat:@"%@,everything went OK!",nameField.text];
        } else {
            msg = @"everything went OK!";
        }
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"Some thing done" message:msg delegate:self cancelButtonTitle:@"Phew" otherButtonTitles:nil];
        [alert show];
    }
}
@end
